from tic-tac-toe import GameBoard


def test1_check_for_victory():
    board = GameBoard([1, 2, 3, "O", "O", "O", 7, 8, 9])
    assert board.check_for_victory() is True


def test2_check_for_victory():
    board = GameBoard(["X", 2, 3, "X", 5, 6, "X", 8, 9])
    assert board.check_for_victory() is True


def test3_check_for_victory():
    board = GameBoard(["X", 2, 3, "X", 5, 6, 7, 8, 9])
    assert board.check_for_victory() is False
