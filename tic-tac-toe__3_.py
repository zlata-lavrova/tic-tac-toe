from custom_Error import LargerNumError, SmallerNumError


class GameBoard:
    def __init__(self, game_area):
        self.game_area = game_area
        self.n = 1

    # функция отрисовки/создания поля
    def draw_area(self) -> None:
        print("-----")
        for i in range(3):
            print(*self.game_area[i * 3:i * 3 + 3])
        print("-----")

    # проверка числа игрока на наличие ошибок
    def choice_positions(self) -> int:
        while True:
            try:
                move = int(input("Введите число для хода (1-9): "))
                self.input_positions(move)
            except (ValueError, LargerNumError, SmallerNumError) as error:
                print(error)
                self.draw_area()

            else:
                break

        return move

    # ввод кастомных ошибок
    def input_positions(self, move: int) -> int:
        if move > 9:
            raise LargerNumError
        elif move < 0:
            raise SmallerNumError

        return move

    # проверка на победу
    def check_for_victory(self):
        win_pos = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ]
        for i in win_pos:
            if self.game_area[i[0]] == self.game_area[i[1]] == self.game_area[
                    i[2]]:
                return self.game_area[i[0]]

    # Определяет чья очередь ходить
    def whose_turn_X_or_O(self):
        if self.n > 0:
            print("Ход игрока X")
        else:
            print("Ход игрока O")

    # Функция добавляет на игровое поле значок X/O
    def add_X_or_O(self, move: int) -> None:
        if self.n > 0:
            self.game_area[move] = "X"
        else:
            self.game_area[move] = "O"

    # Смена хода
    def change_XO(self, move: int) -> bool:
        return (self.game_area[move] == "X" or self.game_area[move] == "O")

    # Смена игрока
    def change_player(self) -> None:
        self.n *= -1


def main():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    move = 1
    for i in range(10):
        board.draw_area()
        board.whose_turn_X_or_O()
        move = board.choice_positions()

        if not move:
            break
        if 0 < move < 10:
            move -= 1
            if not board.change_XO(move):
                board.add_X_or_O(move)
                board.change_player()

                if board.check_for_victory():
                    break

    board.draw_area()
    print("Игра закончена!")


if __name__ == "__main__":
    main()
