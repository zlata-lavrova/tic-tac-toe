class LargerNumError(Exception):
    def __str__(self) -> str:
        return "Число больше 9! \n"


class SmallerNumError(Exception):
    def __str__(self) -> str:
        return "Число меньше 1! \n"
        