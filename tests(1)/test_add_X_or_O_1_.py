from tic-tac-toe import GameBoard


def test1_add_X_or_O():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.n = 1
    board.add_X_or_O(0)
    assert board.game_area == ["X", 2, 3, 4, 5, 6, 7, 8, 9]


def test2_add_X_or_O():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.n = -1
    board.add_X_or_O(2)
    assert board.game_area == [1, 2, "O", 4, 5, 6, 7, 8, 9]


def test3_add_X_or_O():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.n = -1
    board.add_X_or_O(0)
    assert board.game_area == ["O", 2, 3, 4, 5, 6, 7, 8, 9]
