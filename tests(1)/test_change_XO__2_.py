from tic-tac-toe import GameBoard


def test1_change_XO():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    assert board.change_XO(0) is False


def test2_change_XO():
    board = GameBoard(["X", 2, 3, 4, 5, 6, 7, 8, 9])
    assert board.change_XO(0) is True


def test3_change_XO():
    board = GameBoard([1, 2, 3, 4, 5, "O", 7, 8, 9])
    assert board.change_XO(5) is True
