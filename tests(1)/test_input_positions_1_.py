from tic-tac-toe import GameBoard


def test1_input_positions():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.move = 3
    board.input_positions(3)
    assert board.move == 3


def test2_input_positions():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.move = 4
    board.input_positions(4)
    assert board.move == 4


def test3_input_positions():
    board = GameBoard([1, 2, 3, 4, 5, 6, 7, 8, 9])
    board.move = 6
    board.input_positions(6)
    assert board.move == 6
